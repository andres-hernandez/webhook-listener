# webhook-listener

Add a user for the daemon

	# adduser --system --group --disabled-login --disabled-password webhook
	# usermod --shell /bin/bash --home /opt/webhook --move-home webhook

Change user and clone the repository to a folder named `listener` in the `$HOME` directory (authentication may be required)

	# su - webhook
	$ git clone <repo-url> listener

Install PIP requirements

	# pip install -r REQUIREMENTS.txt

Install supervisord and copy the configuration, edit the example routes

	# aptitude install supervisord
	# cp /path/to/listener/webhook-supervisord.conf.example /etc/supervisor/conf.d/webhook-supervisord.conf

Install heirloom-mailx in order to send mails

	# aptitude install heirloom-mailx

Generate the keys with ssh-keygen(1) or import them in $HOME/.ssh/ as id_rsa and id_rsa.pub

	# su - webhook
	$ ssh-keygen -b 8192 -t rsa -C "webhook-listener" -f $HOME/.ssh/id_rsa

Copy or export the *public* key to import it as deployment key on the repo

	$ cat $HOME/.ssh/id_rsa.pub

Run manually the script as the `webhook` user, this will generate the neccessary ssh config

	# su - webhook
	$ cd listener
	$ ./run.sh

When you see the following message, Press Control+C to stop the daemon

	 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)

Check the status with `supervisorctl`

	# supervisorctl status
	webhook-listener                 STOPPED

Start, stop or restart manually with `supervisorctl`

	# supervisorctl start   webhook-listener
	# supervisorctl stop    webhook-listener
	# supervisorctl restart webhook-listener

If supervisord is restarted, the daemon too

	# service supervisor restart

