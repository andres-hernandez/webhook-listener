#!/bin/bash

# ./repo-config.sh - Get information about bitbucket repository
# Andres Hernandez (tonejito)
# November 2015

MKDIR=/bin/mkdir
CURL=/usr/bin/curl
JQ=/usr/bin/jq

# 
API=https://api.bitbucket.org/2.0/repositories

# 
USER=andres-hernandez
# 
if [ -z "$MAIL" ]
then
  MAIL=andres.hernandez@rdigital.mx
fi
# 
REPO=${1}
if [ -z "$REPO" ]
then
  REPO=andres-hernandez/webhook-listener
fi
# 
# Please use trailing slash
if [ -z "$REPO" ]
then
  PREFIX=/tmp/roll/
fi

# Create PREFIX directory if it doesn't exist

if [ ! -d ${PREFIX} ] ; \
then \
  ${MKDIR} -vp ${PREFIX} ; \
fi

# Ask the API about the REPO and parse the result using JQ

${CURL} -sSLk --user ${USER} ${API}/${REPO}/ | \
${JQ} '
{ 
  repositories: 
  [ 
    { 
      dir: ("'${PREFIX}'" + .full_name) ,  
      mail:  
      [ 
        "'${MAIL}'" 
      ] ,  
      clone:  
      [ 
        .links.clone[].href 
      ] ,  
      url: .links.html.href ,  
      full_name: .full_name ,  
      name: .name ,  
      uuid: .uuid 
    } 
  ] 
} 
'

