#!/bin/bash

# Initialize environment before running a ssh-dependent daemon
# Inspired in this post http://stackoverflow.com/a/25020638

MKDIR=/bin/mkdir
TOUCH=/usr/bin/touch
PRINTF=/usr/bin/printf
SSH_KEYGEN=/usr/bin/ssh-keygen
SSH_KEYSCAN=/usr/bin/ssh-keyscan

SSH_DIR=$HOME/.ssh
SSH_CONFIG=$SSH_DIR/config
SSH_KNOWN_HOSTS=$SSH_DIR/known_hosts

WORK_DIR=$HOME/listener
HOSTNAME=bitbucket.org
DAEMON=./listener.py

# Push the old UMASK value
UMASK=`umask`
umask 0077

# Create .ssh dir if it's not already there
if [ ! -d $SSH_DIR ]
then
  $MKDIR -v $SSH_DIR
  $TOUCH $SSH_KNOWN_HOSTS
fi

# Set BatchMode as default client config
if [ ! -e $SSH_CONFIG ]
then
  $PRINTF 'Host *\n\tBatchMode yes\n' > $SSH_CONFIG
fi

# Add the host to the known_hosts file
if [ ! -e $SSH_KNOWN_HOSTS -o -z "`$SSH_KEYGEN -F $HOSTNAME -f $SSH_KNOWN_HOSTS`" ]
then
  $SSH_KEYSCAN -H $HOSTNAME >> $SSH_KNOWN_HOSTS
fi

# Pop the old UMASK value
umask $UMASK
unset  UMASK

# Run the daemon, we should use exec here to make it compatible with supervisord
# http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/
cd $WORK_DIR
exec $DAEMON

