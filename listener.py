#!/usr/bin/env python

# listener.py - Bitbucket WebHook processor
# Andres Hernandez (tonejito)
# November 2015

# Requirements:
#   aptitude install git-core heirloom-mailx

# Notes
#   git pull is executed via ssh, thus we need to have an id_rsa private key
#   and upload the id_rsa.pub public key as "Deployment Key" on each project.
#
#   This can be done programatically via the API (admin rights required)
#
# https://confluence.atlassian.com/bitbucket/deploy-keys-resource-296095243.html
# http://gentlero.bitbucket.org/bitbucket-api/examples/repositories/deploy-keys.html
# https://gist.github.com/arvidj/a942a5264ece8aa631ba

import os
import json
import time
import string
import syslog
import tempfile
from sys import platform as _platform
from subprocess import call, Popen , PIPE , STDOUT
from datetime import datetime
from email.Utils import formatdate
from flask import Flask
from flask import request
app = Flask(__name__)

admin_email = "root@localhost"

# open json datastore
with open('config.json') as data_file:
    config = json.load(data_file)

# pretty-print a json object
def json_print(data):
    print json.dumps(data,sort_keys=True,indent=2,separators=(',',': '))

def displayIntro():
    print 'http://0.0.0.0:5000/webhook'

def displayHTML(request):
    return ''

def search_repo(uuid):
    for r in config['repositories']:
        if uuid == r['uuid']:
            return True
    return False

def get_repo_dir(uuid):
    for r in config['repositories']:
        if uuid == r['uuid']:
            return r['dir']
    return None

def get_repo_mail(uuid):
    for r in config['repositories']:
        if uuid == r['uuid']:
            return r['mail']
    return None

# http://stackoverflow.com/a/13398339
def exec_pull(d):
    cwd = os.getcwd()
    os.chdir(d)
    p = Popen("git pull", stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    os.chdir(cwd)
    return p

def flush_output(p,output,error):
    p.wait()
    # Get stdout and stderr and print them to file
    # https://docs.python.org/2/tutorial/inputoutput.html
    out,err = p.communicate()
    output.write(out)
    error.write(err)
    return

def log_output(repo,hash):
    syslog.openlog(facility=syslog.LOG_USER)
    syslog.syslog(syslog.LOG_INFO,"[%s][%s]" % (repo,hash))
    syslog.closelog()
    return

def mail_output(p,request,repo,author,hash,url,uuid):
    # Open file descriptors
    headers = tempfile.NamedTemporaryFile(prefix='headers.',suffix=".txt")
    body    = tempfile.NamedTemporaryFile(prefix='body.'   ,suffix=".txt")
    output  = tempfile.NamedTemporaryFile(prefix='stdout.' ,suffix=".txt")
    error   = tempfile.NamedTemporaryFile(prefix='stderr.' ,suffix=".txt")
    # Write the request headers to file for debugging
    headers.write(request.remote_addr + "\n")
    headers.write(request.method + " " + request.url + "\n")
    headers.write(str(request.headers))
    flush_output(p,output,error)
    # Get current time in ISO and RFC2822 format
    # http://stackoverflow.com/a/3453266
    # body.write('timestamp_iso:\t%s\n' % datetime.now().isoformat())
    body.write('timestamp_rfc:\t%s\n' % formatdate(time.time()))
    body.write('repository:\t%s\n' % repo)
    body.write('commit_author:\t%s\n' % author)
    body.write('commit_hash:\t%s\n' % hash)
    body.write('commit_url:\t%s\n' % url)
    # Message subject
    subject = "[%s][%s]" % (repo,hash)
    # Send mail to admin
    recipients = admin_email
    # Attach messages only if there was an error
    attachments = ""
    if p.returncode != 0:
        attachments = "-a %s -a %s -a %s" % (headers.name,output.name,error.name)
    # Flush files before sending
    for f in (headers,body,output,error):
        f.flush()
    # Please use heirloom-mailx, bsd-mailx does not support sending attachments
    p = Popen('/usr/bin/heirloom-mailx -s "%s" %s -- %s < %s' % (subject,attachments,recipients,body.name), stdin=None, stdout=None, stderr=None, shell=True)
    # Close file descriptors when the mailx process is done
    p.wait()
    for f in (headers,body,output,error):
        f.close()
    return


def slack_payload(commit_message,color,status,code,repository_url,repository,commit_branch,commit_author,commit_url,commit_hash):
  fallback = pretext = "commit [<%s|%s>] on branch [%s] in repository [<%s|%s>] by [%s]" % (commit_url,commit_hash,commit_branch,repository_url,repository,commit_author)
  title = "git pull"
  return """payload=
{
    "attachments": [
        {
            "fallback": "%s",
            "pretext": "%s",
            "title": "%s",
            "text": "%s",
            "color": "%s",
            "fields":
            [
                {
                    "title": "status",
                    "value": "%s",
                    "short": true
                },
                {
                    "title": "code",
                    "value": "%s",
                    "short": true
                },
                {
                    "title": "repository",
                    "value": "<%s|%s>",
                    "short": true
                },
                {
                    "title": "branch",
                    "value": "%s",
                    "short": true
                },
                {
                    "title": "author",
                    "value": "%s",
                    "short": true
                },
                {
                    "title": "commit",
                    "value": "<%s|%s>",
                    "short": true
                }
            ]
        }
    ]
}""" % (fallback,pretext,title,commit_message,color,status,code,repository_url,repository,commit_branch,commit_author,commit_url,commit_hash)

# Send notification to #slack via shell using curl
def slack_notification(message,status,code,repository_url,repository,commit_branch,commit_author,commit_url,commit_hash):
    # Message color depends on status
    if status == "OK":
      color = "good"
    if status == "Warning":
      color = "warning"
    if status == "Error":
      color = "danger"
    if status == "":
      color = "#c0c0c0"

    url = 'https://hooks.slack.com/services/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'
    payload = slack_payload(message,color,status,code,repository_url,repository,commit_branch,commit_author,commit_url,commit_hash)
    p = Popen("curl -vk# -X POST --data-urlencode '%s' '%s'" %(payload,url), stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    p.wait()

# Validate POST payload
def validate(data):
    try:
        data['repository']['uuid']
        data['repository']['full_name']
        data['repository']['links']['html']['href']
        data['actor']['username']
        data['push']['changes'][0]['new']['target']['hash'][:7]
        data['push']['changes'][0]['new']['target']['links']['html']['href']
        data['push']['changes'][0]['new']['target']['message']
        data['push']['changes'][0]['commits'][0]['message']
        data['push']['changes'][0]['new']['name']
    except Exception as exception:
        print exception
        return False
    return True

@app.route('/', methods=['GET'])
def index():
    return '' , 400

@app.route('/webhook', methods=['GET', 'POST'])
def tracking():
    code = 500
    status = ""
    slack_notify=False
    try:
        if request.method == 'POST':
            data = request.get_json()
            if (validate(data)):
                slack_notify=True
                # Get the repository data from the request
                repository_uuid = data['repository']['uuid']
                commit_repository = data['repository']['full_name']
                commit_repository_url = data['repository']['links']['html']['href']
                commit_author = data['actor']['username']
                commit_hash = data['push']['changes'][0]['new']['target']['hash'][:8]
                commit_url = data['push']['changes'][0]['new']['target']['links']['html']['href']
                commit_message = data['push']['changes'][0]['new']['target']['message']
                message = data['push']['changes'][0]['commits'][0]['message']
                commit_branch =  data['push']['changes'][0]['new']['name']
                # Find repo in config file
                if search_repo(repository_uuid):
                    repository_dir = get_repo_dir(repository_uuid)
                    # Execute pull and send report via email
                    r = exec_pull(repository_dir)
                    log_output(commit_repository,commit_hash)
                    mail_output(r,request,commit_repository,commit_author,commit_hash,commit_url,repository_uuid)
                    if r.returncode == 0:
                        message = "200 OK"
                        # Standard response for successful HTTP requests
                        (status,code) = ('OK' , 200)
                    else:
                        # git-pull returned an error (the mail will have attachments)
                        message = "424 Failed Dependency"
                        # The request failed due to failure of a previous request
                        (status,code) = ('Error' , 424)
                else:
                    # Repository was not found con config file
                    message = "412 Precondition Failed"
                    # The server does not meet one of the preconditions that the requester put on the request
                    (status,code) = ('Warning' , 412)
            else:
                message = "400 Bad Request"
                # The server cannot or will not process the request due to something that is perceived to be a client error
                (status,code) = ('Warning' , 400)
        else:
            # Request method is not "POST /webhook"
            message = "405 Method Not Allowed"
            # A request was made of a resource using a request method not supported by that resource
            (status,code) = ('Warning' , 405)
    except Exception as exception:
        print exception
        message = "500 Internal Server Error"
        # A generic error message, given when an unexpected condition was encountered and no more specific message is suitable
        (status,code) = ('Error' , 500)

    # Send notification to #slack if the POST data was valid
    if slack_notify:
      slack_notification(commit_message,status,code,commit_repository_url,commit_repository,commit_branch,commit_author,commit_url,commit_hash)

    status += '\n'
    return status,code

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)

